# frozen_string_literal: true

#require "version"
require 'aws-sdk'
require 'base64'
require 'json'

module Aws
  module Sesocio
    module Secrets

      class << self
        attr_accessor :key, :secret, :environment, :repo_name
      end
      
      def self.setup(&block)
        yield self
      end

      def self.get_parameters(repo)
        response = Aws::SSM::Client.new.get_parameter(name: "#{environment}-#{repo}").dig(:parameter, :value)
        response = JSON.parse(response)
        response.each do |key, value|
          ENV[key] = value
        end
      rescue
        "Permission problem: check the configuration before continue."
      end

      def self.get_secrets(repo)
        secret_name = "#{self.environment}-#{repo}-secret"
        region_name = "us-east-1"
        client = Aws::SecretsManager::Client.new(region: region_name)
        begin
          get_secret_value_response = client.get_secret_value(secret_id: secret_name)
        rescue Aws::SecretsManager::Errors::DecryptionFailure => e
          raise
        rescue Aws::SecretsManager::Errors::InternalServiceError => e
          raise
        rescue Aws::SecretsManager::Errors::InvalidParameterException => e
          raise
        rescue Aws::SecretsManager::Errors::InvalidRequestException => e
          raise
        rescue Aws::SecretsManager::Errors::ResourceNotFoundException => e
          raise
        else
          if get_secret_value_response.secret_string
            JSON.parse(get_secret_value_response.secret_string).each do |key, value|
              ENV[key.to_s] = value
            end
          else
            ENV.merge!(Base64.decode64(get_secret_value_response.secret_binary))
          end
        end

      end

      def self.populate_env
        self.repo_name.each do |repo|
          get_parameters(repo)
          get_secrets(repo)
        end
      end
    end
  end
end
