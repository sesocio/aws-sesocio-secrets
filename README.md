# Aws::Sesocio::Secrets

Welcome to your new gem! In this directory, you'll find the files you need to be able to package up your Ruby library into a gem. Put your Ruby code in the file `lib/aws/sesocio/secrets`. To experiment with that code, run `bin/console` for an interactive prompt.

TODO: Delete this and the text above, and describe your gem

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'aws-sesocio-secrets'
```

And then execute:

    $ bundle install

Or install it yourself as:

    $ gem install aws-sesocio-secrets

## Usage

Configuration steps:

First of all you need to create a config/initalizers/secrets.rb file with the following lines


```ruby
Aws::Sesocio::Secrets.setup do |config|
  config.key = <Your AWS_ACCESS_KEY_ID>
  config.secret = <Your AWS_SECRET_ACCESS_KEY>
  config.environment = <staging | development | production | test>
  config.repo_name = [< core | coupons | assitance ...>]
end
```

After that, you need to put the next line in application.rb


```ruby 
Aws::Sesocio::Secret.populate_env
```

And that's all!

## Development

After checking out the repo, run `bin/setup` to install dependencies. You can also run `bin/console` for an interactive prompt that will allow you to experiment.

To install this gem onto your local machine, run `bundle exec rake install`. To release a new version, update the version number in `version.rb`, and then run `bundle exec rake release`, which will create a git tag for the version, push git commits and the created tag, and push the `.gem` file to [rubygems.org](https://rubygems.org).

## Contributing

Bug reports and pull requests are welcome on GitHub at https://github.com/[USERNAME]/aws-sesocio-secrets.
