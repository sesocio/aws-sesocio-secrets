require 'json'
RSpec.describe Aws::Sesocio::Secrets do
    context "initialize the app" do
        it "need to be configurable" do
            described_class.setup do |config|
                config.key = "TESTKEY"
                config.secret = "TESTSECRET"
                config.repo_name = "core"
                config.environment = "staging"
            end
            expect(described_class.key).to eql("TESTKEY")
            expect(described_class.secret).to eql("TESTSECRET")
            expect(described_class.repo_name).to eql("core")
            expect(described_class.environment).to eql("staging")
        end
    end

    context "must be able to get data from aws-cli" do

        before(:all) do
            described_class.setup do |config|
                config.key = ENV["AWS_SECRET_ACCESS_KEY"]
                config.secret = ENV["AWS_ACCESS_KEY_ID"]
                config.repo_name = "core"
                config.environment = "staging"
            end
        end
        it "can get parameters" do
            cmd = JSON.parse(`aws ssm get-parameters --names 'staging-core'`)
            expect(cmd.class).to be(Hash)
            expect(cmd).to have_key("Parameters")
        end

        it "must merge parameters with ENV" do
            initial_size = ENV.keys.count 
            described_class.get_parameters
            expect(ENV.keys.count > initial_size).to be(true)
        end

        it "must merge secrets with ENV" do
            initial_size = ENV.keys.count
            described_class.get_secrets
            expect(ENV.keys.count > initial_size).to be(true)
        end
    end
end